# Automated Repo Setup

Learn more about the automated-repo-setup and other divekit tools in the [Divekit documentation](https://divekit.github.io/docs/automated-repo-setup/).

The Automated Repo Setup (ARS) is a Node.js-based tool for the automated creation and management of Git repositories for students. It is part of the Divekit toolset and works with TypeScript.

## Architecture

The tool consists of several main components:

### 1. Service Layer
```typescript
const creator = new RepositoryCreator(
    config,              // Konfiguration
    repositoryAdapter,   // Repository-Adapter
    contentProvider,     // Content-Provider
    logger              // Logger
)
```

```mermaid
sequenceDiagram
    Client->>RepositoryCreator: generateRepositories()
    RepositoryCreator->>ContentRetriever: retrieveOriginFiles()
    RepositoryCreator->>RepositoryAdapter: prepareEnvironment()
    RepositoryCreator->>ContentProvider: provideRepositoriesWithContent()
    RepositoryCreator->>OverviewGenerator: generateOverviewPage()
```

### 2. Repository Adapter
- Abstraction for repository operations
- Implementations for GitLab and local file system
- Manages code and test repositories

```typescript
interface RepositoryAdapter {
    createCodeRepository(name: string): Promise<void>;
    createTestRepository(name: string): Promise<void>;
    addMembersToCodeRepository(members: string[]): Promise<void>;
    linkCodeAndTestRepository(): Promise<void>;
}
```

### 3. Content-Manager
- ContentRetriever: Loads and filters origin repository files
- ContentProvider: Distributes content to code and test repositories
- Supports solution file filtering

```mermaid
flowchart LR
    A[Origin Files] --> B[Load Files]
    B --> C[Filter Solutions]
    C --> D[Process Content]
    D --> E[Code Repo]
    D --> F[Test Repo]
```

### 4. Task-Queue
- Manages parallel repository creation
- Implements retry mechanisms
- Monitors progress

```typescript
class TaskQueue<T> {
    private tasks: Task<T>[];
    private maxWorkers: number;

    async runTasks(): Promise<T[]> {
        // Parallel execution with progress monitoring
    }
}
```

### 5. Configuration
The configuration is done via JSON files:

```json
{
    "general": {
        "localMode": true,
        "createTestRepository": true,
        "variateRepositories": true,
        "maxConcurrentWorkers": 1,
        "globalLogLevel": "Info"
    },
    "repository": {
        "repositoryName": "st2-praktikum",
        "repositoryMembers": [["student1"]]
    },
    "remote": {
        "originRepositoryId": 1012,
        "codeRepositoryTargetGroupId": 161,
        "testRepositoryTargetGroupId": 170
    }
}
```

## Usage

### 1. Installation

```bash
# Via npm
npm install @divekit/automated-repo-setup

# Oder lokal
npm install
npm run build
```

### 2. Create configuration

```typescript
// Load configuration
const config = ConfigManager.getInstance();
config.loadConfigsFromOriginRepoFiles(files);

// Initialize repository adapter
const adapter = new GitlabRepositoryAdapter(individualRepo);

// Create content provider
const provider = new ContentProvider(adapter, individualRepo);
```

### 3. Generate repositories

```typescript
import { RepositoryCreator } from '@divekit/automated-repo-setup';

const main = async () => {
    try {
        const creator = new RepositoryCreator();
        await creator.generateRepositories();
    } catch (error) {
        Logger.getInstance().error(error);
    }
};

main();
```

## Extension

### 1. Custom repository adapter

```typescript
class CustomRepositoryAdapter implements RepositoryAdapter {
    async createCodeRepository(name: string): Promise<void> {
        // Implement repository creation
    }
    
    async addMembersToCodeRepository(members: string[]): Promise<void> {
        // Implement member management
    }

    async linkCodeAndTestRepository(): Promise<void> {
        // Implement repository linking
    }
}
```

### 2. Content-Provider extension

```typescript
class CustomContentProvider extends ContentProvider {
    protected async filterIndividualRepositoryFiles(files: RepositoryFile[]): RepositoryFile[] {
        // Implement custom filter logic
        return files;
    }

    protected async processContent(files: RepositoryFile[]): RepositoryFile[] {
        // Implement custom content processing
        return files;
    }
}
```

## Best Practices

1. **Repository-Structure**
   - Separate code and test repositories cleanly
   - Use consistent naming schemes
   - Keep configurations separate

2. **Error handling**
   - Use the logger system for debugging
   - Implement retry mechanisms for GitLab operations
   - Validate configurations before execution

3. **Performance**
   - Use parallel processing for multiple repositories
   - Optimize file filtering
   - Cache frequently used resources

## Error handling

```typescript
try {
    // Repository creation
    await adapter.createCodeRepository(repoName);
    Logger.getInstance().info(`Created repository ${repoName}`);

    // Content processing
    const files = await provider.processContent(originFiles);
    if (!files.length) {
        throw new Error('No files to process');
    }

    // Add members
    await adapter.addMembersToCodeRepository(members);
} catch (error) {
    Logger.getInstance().error(`Failed to setup repository: ${error.message}`);
    throw error;
}
```
