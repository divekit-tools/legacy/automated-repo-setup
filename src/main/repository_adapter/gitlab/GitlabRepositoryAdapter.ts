import * as dotenv from 'dotenv'
import * as readline from 'node:readline/promises';
import { URL } from 'node:url';
import { RepositoryAdapter } from "../RepositoryAdapter";
import { RepositoryFile } from "../../content_manager/RepositoryFile";
import {
    AccessLevel,
//    CommitSchema,
    Gitlab,
    UserSchema,
    ProjectVariableSchema,
    ProjectSchema
} from "@gitbeaker/rest";
import { ConfigManager } from "../../config/ConfigManager";
import { EncodingRetriever } from "../../content_manager/EncodingRetriever";
import { IndividualRepository } from "../../repository_creation/IndividualRepository";
import { Logger } from "../../logging/Logger";

dotenv.config();
const gitlab = new Gitlab({
    host: process.env.HOST,
    token: process.env.API_TOKEN!,
});

// Get main branch name from environment variable or default to "master"
const mainBranch = process.env.DIVEKIT_MAINBRANCH_NAME || "master";

/**
 * Implementation of RepositoryAdapter for GitLab repositories.
 * Manages repositories using the GitLab REST API, supporting full GitLab functionality
 * including member management, repository linking, and CI/CD pipeline configuration.
 */
export class GitlabRepositoryAdapter implements RepositoryAdapter { // TODO create POJOs for any types

    /** Reference to the created code repository */
    private codeRepository: ProjectSchema | null = null;
    
    /** Reference to the created test repository */
    private testRepository: ProjectSchema | null = null;

    /** Configuration for repository operations */
    private repositoryConfig = ConfigManager.getInstance().getRepositoryConfig();

    /**
     * Creates a new GitlabRepositoryAdapter instance.
     * 
     * @param individualRepository - Optional repository containing individual-specific information
     */
    constructor(private individualRepository?: IndividualRepository) {
    }

    /**
     * Retrieves all files from the origin repository using GitLab's API.
     * Handles both binary and text files appropriately.
     * 
     * @returns Promise resolving to array of repository files
     */
    public async retrieveOriginFiles(): Promise<RepositoryFile[]> {
        let tree: any = await gitlab.Repositories.allRepositoryTrees(this.repositoryConfig.remote.originRepositoryId,
            {recursive: true, orderBy: "path", perPage: 100, sort: "asc", ref: mainBranch});
        let repositoryFiles: RepositoryFile[] = [];

        for (var treeFile of tree) {
            if (treeFile.type === "blob") { // process only files not directories
                if (EncodingRetriever.isBlob(treeFile.path)) {
                    let file = await gitlab.RepositoryFiles.show(this.repositoryConfig.remote.originRepositoryId, treeFile.path, mainBranch);
                    repositoryFiles.push({path: treeFile.path, content: file.content, encoding: file.encoding});
                } else {
                    let fileContent: any = await gitlab.RepositoryFiles.showRaw(this.repositoryConfig.remote.originRepositoryId, treeFile.path, mainBranch);
                    repositoryFiles.push({path: treeFile.path, content: fileContent});
                }
            }
        }

        return repositoryFiles;
    }

    /**
     * Creates a code repository. Looks if the repository already exists and creates it if it doesn't.
     * 
     * @param repositoryName The name of the repository
     */
    public async createCodeRepository(repositoryName: string): Promise<void> {
        let existingRepository = await this.searchForRepository(repositoryName, this.repositoryConfig.remote.codeRepositoryTargetGroupId);
        this.codeRepository = existingRepository ? existingRepository :
            await gitlab.Projects.create({namespaceId: this.repositoryConfig.remote.codeRepositoryTargetGroupId, name: repositoryName}) as ProjectSchema;
    }

    public async createTestRepository(repositoryName: string): Promise<void> {
        let existingRepository = await this.searchForRepository(repositoryName, this.repositoryConfig.remote.testRepositoryTargetGroupId);
        this.testRepository = existingRepository ? existingRepository :
            await gitlab.Projects.create({namespaceId: this.repositoryConfig.remote.testRepositoryTargetGroupId, name: repositoryName}) as ProjectSchema;
    }

    public async addMembersToCodeRepository(members: string[] | undefined): Promise<void> {
        if (members) {
            for (let i = 0; i < members.length; i++) {
                let user = await this.searchForUser(members[i]);
                if (user) {
                    try {
                        await gitlab.ProjectMembers.add(this.codeRepository!.id, user.id, this.getAccessLevel());
                        Logger.getInstance().info(`Added User ${user.username} to project`, this.individualRepository!.id!, true);
                    } catch (error) {
                        Logger.getInstance().warning(`Could not add user ${members[i]}`, this.individualRepository!.id!, true);
                        Logger.getInstance().warning(<any> error, this.individualRepository!.id!, true);
                    }
                } else {
                    Logger.getInstance().warning(`Could not find user ${members[i]} by name`, this.individualRepository!.id!, true);
                }
            }
        }
    }

    /**
     * Searches for an existing repository in the specified group or creates a new one.
     * 
     * @param variableValues - List of patterns to check against
     * @param checkVariableValue - Value to check for pattern matches
     * @returns true if any pattern matches the value, false otherwise
     */
    private async searchForRepository(repositoryName: string, groupId: number): Promise<ProjectSchema | null> {
        let foundProjects = (await gitlab.Projects.search(repositoryName, {showExpanded: true})).data as ProjectSchema[];

        for (let foundProject of foundProjects) {
            if (foundProject.namespace.id == groupId) {
                return foundProject;
            }
        }
        return null;
    }

    /**
     * Searches for a GitLab user by username.
     * 
     * @param userName - Username to search for
     * @returns Promise resolving to found user or null if not found
     */
    private async searchForUser(userName: string): Promise<UserSchema | null> {
        let users: UserSchema[] = await gitlab.Search.all("users", userName) as UserSchema[];
        if (users.length == 0) {
            return null;
        }

        for (let i = 0; i < users.length; i++) {
            if (users[i].username === userName) {
                return users[i];
            }
        }
        return null;
    }

    public async addOverviewToOverviewRepository(overviewContent: RepositoryFile): Promise<void> {
        let commitActions: any[] = [{ action: 'create', filePath: overviewContent.path, content: overviewContent.content }];

        await gitlab.Commits.create(this.repositoryConfig.overview.overviewRepositoryId, mainBranch, "add overview file", commitActions);
    }

    /**
     * Sets the inbound token permissions for the code repository.
     * This allows the test repository's CI/CD pipeline to access the code repository.
     * Direction INBOUND means: code repository allows incoming access from test repository.
     * 
     * Since there is no REST-API endpoint for changing the job token scope,
     * we need to manually fire a GraphQL request.
     * 
     * @returns A promise that resolves when the inbound token permissions are set
     */
    private async setInboundTokenPermissions(): Promise<void> {
        // FIXME: Since there is no REST-API endpoint for changing the job token scope,
        //        we need to manually fire a GraphQL request.
        // https://gitlab.com/gitlab-org/gitlab/-/issues/351740

        let url = new URL("/api/graphql", process.env.HOST!);

        const postData = `{"variables":null,"query":"mutation{ciJobTokenScopeAddProject(input:{direction:INBOUND,projectPath:\\"${this.codeRepository!.path_with_namespace}\\",targetProjectPath:\\"${this.testRepository!.path_with_namespace}\\"}){errors}}"}`;

        const response = await fetch(url, {
            method: "POST",
            body: postData,
            headers: {
                "Authorization": `Bearer ${process.env.API_TOKEN}`,
                "Content-Type": "application/json"
            }
        });

        const json = await response.json();
        if ((json.data.ciJobTokenScopeAddProject.errors as any[]).length > 0) {
            const errorMessages = json.data.ciJobTokenScopeAddProject.errors
                .map(err => {
                    // Assuming the error objects might have a message property, or could be just strings
                    return typeof err === 'object' && err.message ? err.message : err.toString();
                })
                .join(", ");
            throw new Error("Errors occurred while setting inbound permissions: " + errorMessages);
        }
    }

    
    /**
     * Links the code repository with the test repository by setting environment variables 
     * and creating pipeline triggers. This method checks if the required variables already 
     * exist and creates them if necessary.
     * 
     * The following variables are set:
     * - CODE_REPO_URL: URL of the code repository
     * - CODE_REPO_TOKEN: Token for accessing the code repository
     * - TEST_REPO_TRIGGER_URL: URL to trigger the pipeline in the test repository
     * - TEST_REPO_TRIGGER_TOKEN: Token to trigger the pipeline in the test repository
     * 
     * Additionally, the `setInboundTokenPermissions` method is called to set the permissions 
     * for the job token.
     */
    public async linkCodeAndTestRepository(): Promise<void> {
        // Set Environment Variables and create pipeline trigger
        if (!(await this.doesVariableExistInRepository(this.testRepository!, 'CODE_REPO_URL'))) {
            await gitlab.ProjectVariables.create(this.testRepository!.id, 'CODE_REPO_URL', this.codeRepository!.web_url);
        }
        if (!(await this.doesVariableExistInRepository(this.testRepository!, 'CODE_REPO_TOKEN'))) {
            const options = {
                protected: true,
                masked: true
            }
            const projectToken = await gitlab.ProjectAccessTokens.create(this.codeRepository!.id,
                "ACCESS_TOKEN", ['read_api'], "2024-12-31" )
            await gitlab.ProjectVariables.create(this.testRepository!.id, 'CODE_REPO_TOKEN', projectToken.token as string , options);
        }
        if (!(await this.doesVariableExistInRepository(this.codeRepository!, 'TEST_REPO_TRIGGER_URL'))) {
            const triggerURL = `${this.testRepository!._links.self}/trigger/pipeline`;
            await gitlab.ProjectVariables.create(this.codeRepository!.id, 'TEST_REPO_TRIGGER_URL', triggerURL);
        }
        if (!(await this.doesVariableExistInRepository(this.codeRepository!, 'TEST_REPO_TRIGGER_TOKEN'))) {
            const pipelineTrigger: any = await gitlab.PipelineTriggerTokens.create(this.testRepository!.id, "other_project");
            await gitlab.ProjectVariables.create(this.codeRepository!.id, 'TEST_REPO_TRIGGER_TOKEN', pipelineTrigger.token);
        }

        await this.setInboundTokenPermissions();
    }

    /**
     * Checks if a variable exists in a GitLab project's variables.
     * 
     * @param repository - The repository to check
     * @param variableName - Name of the variable to look for
     * @returns Promise resolving to true if variable exists, false otherwise
     */
    private async doesVariableExistInRepository(repository: ProjectSchema, variableName: string): Promise<boolean> {
        let foundVariables = await gitlab.ProjectVariables.all(repository.id) as ProjectVariableSchema[];

        for (let foundVariable of foundVariables) {
            if (foundVariable.key == variableName) {
                return true;
            }
        }

        return false;
    }

    /**
     * Provides content to the code repository by creating an initial commit.
     * Takes an array of repository files and converts them to GitLab commit actions.
     * Each file is properly encoded and added as a 'create' action in the commit.
     * 
     * Note: Due to a known GitLab API issue (Error 500) with empty repositories,
     * the commit existence check is currently skipped.
     * 
     * @param codeRepositoryFiles - Array of files to be committed to the repository,
     *                             each containing path and content information
     * @returns Promise resolving when all files are successfully committed
     */
    public async provideContentToCodeRepository(codeRepositoryFiles: RepositoryFile[]): Promise<void> {
        let codeRepositoryCommitActions = this.convertFileArrayToCommits(codeRepositoryFiles);

        // this Gitlab API call seems to produce error 500 consistently
        // (try https://git.archi-lab.io/api/v4/projects/<id>/repository/commits with <id> being a valid project id,
        //  for a freshly created, EMPTY repo - it will return 500)
        // Therefore we skip this call.
        //if (!(await this.IsAtLeastOneCommitInRepository(this.codeRepository!))) {
            await gitlab.Commits.create(this.codeRepository!.id, mainBranch, "initial commit", codeRepositoryCommitActions);
        //}
    }

    /**
     * Provides content to the test repository by creating an initial commit.
     * Takes an array of repository files and converts them to GitLab commit actions.
     * Each file is properly encoded and added as a 'create' action in the commit.
     * 
     * Note: Due to a known GitLab API issue (Error 500) with empty repositories,
     * the commit existence check is currently skipped.
     * 
     * @param testRepositoryFiles - Array of files to be committed to the repository,
     *                             each containing path and content information
     * @returns Promise resolving when all files are successfully committed
     */
    public async provideContentToTestRepository(testRepositoryFiles: RepositoryFile[]): Promise<void> {
        let testRepositoryCommitActions = this.convertFileArrayToCommits(testRepositoryFiles);

        // this Gitlab API call seems to produce error 500 consistently
        // (try https://git.archi-lab.io/api/v4/projects/<id>/repository/commits with <id> being a valid project id,
        //  for a freshly created, EMPTY repo - it will return 500)
        // Therefore we skip this call.
        //if (this.testRepository && testRepositoryCommitActions.length > 0 && !(await this.IsAtLeastOneCommitInRepository(this.testRepository!))) {
            await gitlab.Commits.create(this.testRepository!.id, mainBranch, "initial commit", testRepositoryCommitActions);
        //}
    }

    /**
     * Converts repository files to GitLab commit actions.
     * Ensures proper encoding for all files.
     * 
     * @param repositoryFiles - Files to convert to commit actions
     * @returns Array of GitLab commit actions
     */
    private convertFileArrayToCommits(repositoryFiles: RepositoryFile[]): any[] {
        repositoryFiles = EncodingRetriever.replaceFileEncoding(repositoryFiles, "text");

        let commitActions: any[] = [];
        for (var file of repositoryFiles) {
            commitActions.push({ action: 'create', filePath: file.path, content: file.content, encoding: file.encoding });
        }
        return commitActions;
    }

    /*
    private async IsAtLeastOneCommitInRepository(repository: ProjectSchema): Promise<boolean> {
        let commits = await gitlab.Commits.all(repository.id) as CommitSchema[];
        return commits.length > 0;
    }
    */

    /**
     * Generates a link to the test page for the repository.
     * Transforms the repository URL into a GitLab Pages URL format.
     * If no test repository exists, uses the code repository URL as base.
     * 
     * Example transformation:
     * From: https://git.example.com/group/project
     * To:   http://project.pages.example.com/group/project
     * 
     * @returns The formatted GitLab Pages URL for accessing the test page
     */
    public getLinkToTestPage(): string {
        let link;
        if (this.testRepository == null) {
            link = this.codeRepository!.web_url;
        } else {
            link = this.testRepository.web_url;
        }

        let linkParts = link.split("/");
        let domainParts = linkParts[2].split(".");
        let overviewLink = "http://" + linkParts[3] + ".pages";

        for (let i = 1; i < domainParts.length; i++) {
            overviewLink = overviewLink + "." + domainParts[i];
        }

        for (let i = 4; i < linkParts.length; i++) {
            overviewLink = overviewLink + "/" + linkParts[i];
        }

        return overviewLink;
    }

    /**
     * Determines the appropriate access level for repository members.
     * Based on configuration setting for adding users as guests.
     * 
     * @returns AccessLevel enum value for repository members
     */
    private getAccessLevel(): number {
        if (this.repositoryConfig.remote.addUsersAsGuests) {
            return AccessLevel.GUEST;
        } else {
            return AccessLevel.MAINTAINER;
        }
    }

    public getLinkToCodeRepository(): string {
        return this.codeRepository!.web_url;
    }

    public getLinkToTestRepository(): string {
        return this.testRepository!.web_url;
    }

    /**
     * Prepares the GitLab environment by optionally cleaning up existing repositories.
     * Requires user confirmation before deletion.
     */
    public async prepareEnvironment() { 
        if (this.repositoryConfig.remote.deleteExistingRepositories) {
            const rl = readline.createInterface({input: process.stdin, output: process.stdout});

            const answer = await rl.question(`You are about to delete all repositories associated with this environment.\n  Continue? [y/N] `);
            rl.close();
          
            if(answer !== 'y') {
              console.log("User terminated environment preparation");
              process.exit();
            }
          
            console.log("Clearing current environment..");
            await this.deleteProjectsInGroup(this.repositoryConfig.remote.codeRepositoryTargetGroupId);

            if (this.repositoryConfig.general.createTestRepository) {
                await this.deleteProjectsInGroup(this.repositoryConfig.remote.testRepositoryTargetGroupId);
            }
        }
    }

    /**
     * Deletes all projects in a specified GitLab group.
     * Used during environment preparation.
     * 
     * @param groupId - ID of the group to clean up
     */
    private async deleteProjectsInGroup(groupId: number) {
        let projects = await gitlab.Groups.allProjects(groupId);
        for (let project of projects) {
            await gitlab.Projects.remove(project.id);
        }
    }
}
