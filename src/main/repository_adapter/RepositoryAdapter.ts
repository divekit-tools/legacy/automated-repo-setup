import { RepositoryFile } from "../content_manager/RepositoryFile";

/**
 * Adapter interface for interacting with repository systems.
 * Provides methods for managing code and test repositories, including creation,
 * content management, and linking operations.
 */
export interface RepositoryAdapter {
    /**
     * Prepares the environment for repository operations.
     * Should be called before any other operations.
     */
    prepareEnvironment(): Promise<void>;

    /**
     * Retrieves all files from the origin repository.
     * 
     * @returns Promise resolving to array of repository files
     */
    retrieveOriginFiles(): Promise<RepositoryFile[]>;

    /**
     * Creates a new code repository with the specified name.
     * 
     * @param repositoryName - Name for the new code repository
     */
    createCodeRepository(repositoryName: string): Promise<void>;

    /**
     * Creates a new test repository with the specified name.
     * 
     * @param repositoryName - Name for the new test repository
     */
    createTestRepository(repositoryName: string): Promise<void>;

    /**
     * Adds specified members to the code repository with appropriate permissions.
     * 
     * @param members - Array of member identifiers to add, undefined if no members to add
     */
    addMembersToCodeRepository(members: string[] | undefined): Promise<void>;

    /**
     * Establishes a link between the code and test repositories.
     * Should be called after both repositories are created.
     */
    linkCodeAndTestRepository(): Promise<void>;

    /**
     * Provides content files to the code repository.
     * 
     * @param codeRepositoryFiles - Array of files to add to the code repository
     */
    provideContentToCodeRepository(codeRepositoryFiles: RepositoryFile[]): Promise<void>;

    /**
     * Provides content files to the test repository.
     * 
     * @param testRepositoryFiles - Array of files to add to the test repository
     */
    provideContentToTestRepository(testRepositoryFiles: RepositoryFile[]): Promise<void>;

    /**
     * Adds overview content to a separate overview repository.
     * 
     * @param overviewContent - Content to add to the overview repository
     */
    addOverviewToOverviewRepository(overviewContent: RepositoryFile): Promise<void>;

    /**
     * Retrieves the URL or path to access the code repository.
     * 
     * @returns Link to the code repository
     */
    getLinkToCodeRepository(): string;

    /**
     * Retrieves the URL or path to access the test repository.
     * 
     * @returns Link to the test repository
     */
    getLinkToTestRepository(): string;

    /**
     * Retrieves the URL or path to access the test page.
     * 
     * @returns Link to the test page
     */
    getLinkToTestPage(): string;
}