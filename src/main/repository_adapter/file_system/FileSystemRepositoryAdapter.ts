import { RepositoryFile } from "../../content_manager/RepositoryFile";
import * as fs from 'fs';
import * as path from 'path';
import { ConfigManager } from "../../config/ConfigManager";
import { RepositoryFileLoader } from "../RepositoryFileLoader";
import { RepositoryAdapter } from "../RepositoryAdapter";
import { Logger } from "../../logging/Logger";

/**
 * Implementation of RepositoryAdapter for local file system operations.
 * Manages repositories as directories in the local file system, primarily used for testing and development.
 * Does not support certain features like member management or repository linking.
 */
export class FileSystemRepositoryAdapter implements RepositoryAdapter {

    /** Path to the main test folder containing all repository data */
    private readonly testFolder = path.join('./resources/test');
    
    /** Path to the input folder containing original repository files */
    private readonly inputFolder = path.join(this.testFolder, 'input');
    
    /** Path to the output folder containing generated repositories */
    private readonly outputFolder = path.join(this.testFolder, 'output');
    
    /** Path to the folder representing the code repository */
    private readonly codeRepoFolder = path.join(this.outputFolder, 'code');
    
    /** Path to the folder representing the test repository */
    private readonly testRepoFolder = path.join(this.outputFolder, 'test');

    /** Path to the current code repository instance */
    private codeRepository?: string;
    
    /** Path to the current test repository instance */
    private testRepository?: string;

    /** Configuration for repository operations */
    private repositoryConfig = ConfigManager.getInstance().getRepositoryConfig();

    /**
     * Creates a new FileSystemRepositoryAdapter instance.
     * Initializes the required folder structure.
     */
    constructor() {
        this.initializeTestFolder();
    }

    /**
     * Prepares the environment by cleaning up the output folder.
     * Removes all existing repository content for a fresh start.
     */
    public async prepareEnvironment() {
        await fs.promises.rm(this.outputFolder, { recursive: true });
    }

    /**
     * Creates the basic folder structure required for file system operations.
     * Ensures input and output folders exist.
     */
    private initializeTestFolder() {
        fs.mkdirSync(this.inputFolder, { recursive: true });

        fs.mkdirSync(this.codeRepoFolder, { recursive: true });
        fs.mkdirSync(this.testRepoFolder, { recursive: true });
    }

    /**
     * Retrieves files from the origin repository in the input folder.
     * If no specific origin path is configured, uses the first directory found in the input folder.
     * 
     * @returns Promise resolving to array of repository files
     * @throws Error if no origin repository is found
     */
    async retrieveOriginFiles(): Promise<RepositoryFile[]> {
        return new Promise((resolve, reject) => {
            let originFolder: string = this.repositoryConfig.local.originRepositoryFilePath;

            if (originFolder.length == 0) {
                let directoryNames = fs.readdirSync(this.inputFolder);
                if (directoryNames.length == 0) {
                    Logger.getInstance().error(`There was no origin repository found in folder: ${this.inputFolder}`);
                    reject();
                }
        
                originFolder = path.join(this.inputFolder, directoryNames[0]);
            }
            
            resolve(RepositoryFileLoader.loadRepositoryFiles(originFolder, originFolder));
        });
    }

    /**
     * Creates a directory representing the code repository.
     * 
     * @param repositoryName - Name of the directory to create
     */
    async createCodeRepository(repositoryName: string): Promise<void> {
        this.codeRepository = path.join(this.codeRepoFolder, repositoryName);
        fs.mkdirSync(this.codeRepository);
    }

    /**
     * Creates a directory representing the test repository.
     * 
     * @param repositoryName - Name of the directory to create
     */
    async createTestRepository(repositoryName: string): Promise<void> {
        this.testRepository = path.join(this.testRepoFolder, repositoryName);
        fs.mkdirSync(this.testRepository);
    }

    /**
     * No-op implementation as member management is not supported in file system mode.
     * 
     * @param _members - Ignored in this implementation
     */
    async addMembersToCodeRepository(_members: string[] | undefined): Promise<void> {
        return; // no student members on file system possible
    }

    /**
     * No-op implementation as overview repository is not supported in file system mode.
     * 
     * @param _overviewContent - Ignored in this implementation
     */
    async addOverviewToOverviewRepository(_overviewContent: RepositoryFile): Promise<void> {
        return; // no overview repository on file system
    }

    /**
     * No-op implementation as repository linking is not supported in file system mode.
     */
    async linkCodeAndTestRepository(): Promise<void> {
        return; // no pipelines on file system possible
    }

    /**
     * Writes files to the code repository directory.
     * Creates necessary subdirectories and preserves the file structure.
     * 
     * @param codeRepositoryFiles - Files to write to the code repository
     */
    async provideContentToCodeRepository(codeRepositoryFiles: RepositoryFile[]): Promise<void> {
        if (this.codeRepository) {
            this.writeFilesToFolder(codeRepositoryFiles, this.codeRepository);
        } 
    }

    /**
     * Writes files to the test repository directory.
     * Creates necessary subdirectories and preserves the file structure.
     * 
     * @param testRepositoryFiles - Files to write to the test repository
     */
    async provideContentToTestRepository(testRepositoryFiles: RepositoryFile[]): Promise<void> {
        if (this.testRepository) {
            this.writeFilesToFolder(testRepositoryFiles, this.testRepository);
        }
    }

    /**
     * Writes repository files to a specified folder, maintaining the directory structure.
     * Creates any necessary subdirectories automatically.
     * 
     * @param repositoryFiles - Files to write
     * @param folderPath - Target folder path
     */
    private writeFilesToFolder(repositoryFiles: RepositoryFile[], folderPath: string): void {
        for (var repositoryFile of repositoryFiles) {
            // Get new file name
            repositoryFile.path = path.join(folderPath, repositoryFile.path);
            // Create folder of file
            let directoryPath = path.dirname(repositoryFile.path);
            fs.mkdirSync(directoryPath, { recursive: true });
            // Write File
            fs.writeFileSync(repositoryFile.path, repositoryFile.content, { encoding: repositoryFile.encoding as BufferEncoding });
        }
    }

    /**
     * Returns the path to the test repository.
     * 
     * @returns Path to test repository or empty string if not created
     */
    getLinkToTestPage(): string {
        return this.testRepository ? this.testRepository : "";
    }

    /**
     * Returns the path to the code repository.
     * 
     * @returns Path to code repository or empty string if not created
     */
    getLinkToCodeRepository(): string {
        return this.codeRepository ? this.codeRepository : "";
    }

    /**
     * Returns the path to the test repository.
     * 
     * @returns Path to test repository or empty string if not created
     */
    getLinkToTestRepository(): string {
        return this.testRepository ? this.testRepository : "";
    }
}