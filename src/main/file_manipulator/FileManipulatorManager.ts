import { FileManipulator } from "./FileManipulator";
import { RepositoryFile } from "../content_manager/RepositoryFile";
import { UmletFileManipulator } from "./UmletFileManipulator";

/**
 * Manages a collection of file manipulators that can process repository files.
 * Coordinates the application of different file manipulators based on their compatibility with files.
 * Currently supports manipulators like UmletFileManipulator for handling specific file types.
 */
export class FileManipulatorManager {

    /** Collection of available file manipulators that can process repository files */
    private fileManipulators: FileManipulator[] = this.getFileManipulators(); 

    /**
     * Processes a repository file through compatible file manipulators.
     * If no manipulator is compatible with the file, returns the original file unchanged.
     * If multiple manipulators are compatible, applies all of them in sequence.
     * 
     * @param repositoryFile - The repository file to be processed
     * @returns Promise resolving to an array of processed repository files. May contain multiple files if manipulators generate additional outputs
     */
    async manipulateRepositoryFile(repositoryFile: RepositoryFile): Promise<RepositoryFile[]> {
        let repositoryFiles: RepositoryFile[] = [];
        let manipulated = false;

        for (var fileManipulator of this.fileManipulators) {
            if (fileManipulator.shouldManipulateRepositoryFile(repositoryFile)) {
                repositoryFiles = repositoryFiles.concat(await fileManipulator.manipulateRepositoryFile(repositoryFile));
                manipulated = true;
            }
        }

        if (!manipulated) {
            repositoryFiles.push(repositoryFile);
        }
        return repositoryFiles;
    }

    /**
     * Initializes and returns the collection of available file manipulators.
     * Currently includes:
     * - UmletFileManipulator: Converts UMLet diagram files to JPG format
     * 
     * @returns Array of initialized file manipulator instances
     */
    getFileManipulators(): FileManipulator[] {
        var fileManipulators: FileManipulator[] = [];
        fileManipulators.push(new UmletFileManipulator());
        return fileManipulators;
    }
}