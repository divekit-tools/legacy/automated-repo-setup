import {RepositoryFile} from "../content_manager/RepositoryFile";
import {ContentRetriever} from "../content_manager/ContentRetriever";
import {GitlabRepositoryAdapter} from "../repository_adapter/gitlab/GitlabRepositoryAdapter";
import {ContentProvider} from "../content_manager/ContentProvider";
import {RepositoryAdapter} from "../repository_adapter/RepositoryAdapter";
import {FileSystemRepositoryAdapter} from "../repository_adapter/file_system/FileSystemRepositoryAdapter";
import {OverviewGenerator} from '../generate_overview/OverviewGenerator';
import {IndividualRepository} from './IndividualRepository';
import {IndividualRepositoryManager} from './IndividualRepositoryManager';
import {ConfigManager} from "../config/ConfigManager";
import {Task, TaskQueue} from "./TaskQueue";
import {Logger} from "../logging/Logger";
import {pathListIncludes} from "../utils/PathComparator";

/**
 * Manages the creation and setup of individual repositories.
 * Coordinates the process of generating repositories from origin content,
 * applying individual variations, and setting up both code and test repositories.
 * Supports parallel processing of multiple repositories and provides progress tracking.
 */
export class RepositoryCreator {
    
    /**
     * Main entry point for repository generation process.
     * Performs the following steps:
     * 1. Retrieves and filters origin repository files
     * 2. Initializes individual repositories
     * 3. Prepares the environment
     * 4. Generates repositories in parallel
     * 5. Optionally creates overview page
     * 
     * @returns Promise that resolves when all repositories are generated
     */
    public async generateRepositories(): Promise<void> {
        let originRepositoryFiles: RepositoryFile[] = await this.retrieveOriginRepositoryFiles();
        originRepositoryFiles = this.filterRepositoryList(originRepositoryFiles)

        let individualRepositoryManager: IndividualRepositoryManager = new IndividualRepositoryManager();
        let individualRepositories: IndividualRepository[] = individualRepositoryManager.getIndividualRepositories();   

        Logger.getInstance().debug("Start preparing environment");
        await this.generateRepositoryAdapter().prepareEnvironment();
        Logger.getInstance().debug("Finished preparing environment");

        let contentProviders = await this.startRepositoryGenerationTasks(originRepositoryFiles, individualRepositories);

        if (ConfigManager.getInstance().getRepositoryConfig().overview.generateOverview) {
            let overviewGenerator = new OverviewGenerator(this.generateRepositoryAdapter());
            overviewGenerator.generateOverviewPage(contentProviders);
        }
    }

    /**
     * Filters repository files based on configured subset paths.
     * Only applies filtering in local mode when subset paths are configured.
     * 
     * @param repositoryFiles - Complete list of repository files
     * @returns Filtered list of repository files based on configuration
     */
    private filterRepositoryList(repositoryFiles: RepositoryFile[]): RepositoryFile[] {
        const config = ConfigManager.getInstance().getRepositoryConfig()
        const subsetPaths = config.local.subsetPaths

        // return all paths, if no subset is configured, or we are not in local mode
        if (!config.general.localMode) return repositoryFiles
        if (!subsetPaths) return repositoryFiles

        return repositoryFiles.filter(it => pathListIncludes(subsetPaths, it.path))
    }

    /**
     * Creates and executes parallel tasks for repository generation.
     * Each task processes one individual repository.
     * Limits concurrent execution based on configuration.
     * 
     * @param originRepositoryFiles - Files from the origin repository to be processed
     * @param individualRepositories - List of individual repositories to create
     * @returns Promise resolving to array of created ContentProvider instances
     */
    private async startRepositoryGenerationTasks(originRepositoryFiles: RepositoryFile[], individualRepositories: IndividualRepository[]): Promise<ContentProvider[]> {
        Logger.getInstance().info("Start generating repositories");
        let startTime = new Date().getTime();

        let contentProviderTasks: Task<ContentProvider | Error>[] = [];

        for (let i = 0; i < individualRepositories.length; i++) {
            contentProviderTasks.push(() => {
                return this.createRepository(
                    originRepositoryFiles, 
                    individualRepositories[i]);
            });
        }

        let contentProviders: ContentProvider[] = [];
        let taskQueue = new TaskQueue<ContentProvider | Error>(contentProviderTasks, ConfigManager.getInstance().getRepositoryConfig().general.maxConcurrentWorkers);
        let results = await taskQueue.runTasks();
        
        for (let result of results) {
            if (!(result instanceof Error)) {
                contentProviders.push(result);
            }
        }

        let finishedTime = new Date().getTime();
        Logger.getInstance().info(`Finished generating repositories (Took ${ ((finishedTime - startTime) / 1000 / 60).toFixed(2) } minutes)`);
        return contentProviders;
    }

    /**
     * Creates a single repository for an individual.
     * Handles the complete process including:
     * - Repository naming
     * - Content provision
     * - Error handling and logging
     * 
     * @param originRepositoryFiles - Files from the origin repository
     * @param individualRepository - Individual repository configuration
     * @returns Promise resolving to configured ContentProvider
     * @throws Error if repository creation fails
     */
    private async createRepository(originRepositoryFiles: RepositoryFile[], individualRepository: IndividualRepository): Promise<ContentProvider> {
        const codeRepositoryName = `${ConfigManager.getInstance().getRepositoryConfig().repository.repositoryName}_group_${individualRepository.id}`;
        const testRepositoryName = `${ConfigManager.getInstance().getRepositoryConfig().repository.repositoryName}_tests_group_${individualRepository.id}`;

        try { 
            let repositoryAdapter = this.generateRepositoryAdapter(individualRepository);
            let contentProvider = new ContentProvider(repositoryAdapter, individualRepository);
            await contentProvider.provideRepositoriesWithContent(originRepositoryFiles, codeRepositoryName, testRepositoryName); 
            Logger.getInstance().info(`Finished generating repository`, individualRepository.id!);
            return contentProvider;
        } catch (error) {
            let errorMsg = `An error occurred while generating repository`;
            if (individualRepository.members) {
                errorMsg = errorMsg + " (Members: " + individualRepository.getMembersList() + ")";
            }
            Logger.getInstance().error(errorMsg, individualRepository.id!);
            Logger.getInstance().error(<any> error, individualRepository.id!);
            throw error;
        }
    }

    /**
     * Retrieves and processes files from the origin repository.
     * Includes loading of configuration files and filtering of solution files.
     * 
     * @returns Promise resolving to processed repository files
     * @throws Error if origin files cannot be retrieved
     */
    private async retrieveOriginRepositoryFiles(): Promise<RepositoryFile[]> {
        try {
            let contentRetriever = new ContentRetriever(this.generateRepositoryAdapter());
            let originRepositoryFiles = await contentRetriever.retrieveOriginFiles();
            ConfigManager.getInstance().loadConfigsFromOriginRepoFiles(originRepositoryFiles);
            originRepositoryFiles = contentRetriever.filterOriginFiles(originRepositoryFiles);
            return originRepositoryFiles;
        } catch (error) {
            Logger.getInstance().error("Could not retrieve origin project");
            throw error;
        }
    }

    /**
     * Creates appropriate repository adapter based on configuration.
     * Supports both local file system and GitLab modes.
     * 
     * @param individualRepository - Optional individual repository for GitLab mode
     * @returns Configured RepositoryAdapter instance
     */
    private generateRepositoryAdapter(individualRepository?: IndividualRepository): RepositoryAdapter {
        if (ConfigManager.getInstance().getRepositoryConfig().general.localMode) {
            return new FileSystemRepositoryAdapter();
        } else {
            return new GitlabRepositoryAdapter(individualRepository);
        }
    }
}
