import * as fs from 'fs';
import * as path from 'path';
import { v4 as uuidv4 } from 'uuid';
import { VariationGenerator } from '../content_variation/VariationGenerator';
import { TimeStampCreator } from '../utils/TimeStampCreator';
import { IndividualRepository } from './IndividualRepository';
import { ConfigManager } from '../config/ConfigManager';
import { Logger } from '../logging/Logger';

/**
 * Manages the lifecycle of individual repositories including creation, persistence, and variation generation.
 * Handles loading of existing repositories, creation of new ones, and application of variations.
 * Supports both member-based and count-based repository creation strategies.
 */
export class IndividualRepositoryManager {

    /** Path to the folder containing persisted individual repository data */
    private readonly individualRepositoriesFolder = path.join('./resources/individual_repositories');

    /** Configuration for repository operations */
    private repositoryConfig = ConfigManager.getInstance().getRepositoryConfig();
    
    /** Generator for creating variations in repository content */
    private variationGenerator: VariationGenerator;

    /**
     * Creates a new IndividualRepositoryManager instance.
     * Initializes the variation generator and ensures storage directory exists.
     */
    constructor() {
        this.variationGenerator = new VariationGenerator();
        this.initializeIndividualRepositoriesFolder();
    }

    /**
     * Creates the directory for storing individual repository data if it doesn't exist.
     */
    private initializeIndividualRepositoriesFolder() {
        fs.mkdirSync(this.individualRepositoriesFolder, { recursive: true });
    }

    /**
     * Persists individual repositories to disk.
     * Uses either a configured filename or generates a timestamp-based name.
     * 
     * @param individualRepositories - Array of repositories to save
     */
    private saveIndividualRepositories(individualRepositories: IndividualRepository[]): void {
        // Get file name
        let filePath;
        if (this.repositoryConfig.individualRepositoryPersist.useSavedIndividualRepositories) {
            filePath = path.join(this.individualRepositoriesFolder, this.repositoryConfig.individualRepositoryPersist.savedIndividualRepositoriesFileName);
        } else {
            filePath = path.join(this.individualRepositoriesFolder, this.generateFileName());
        }
        // Write File
        let fileContent = JSON.stringify(individualRepositories, null, 4);
        fs.writeFileSync(filePath, fileContent, 'utf8');
    }

    /**
     * Generates a unique filename for repository persistence.
     * Includes timestamp for tracking when repositories were created.
     * 
     * @returns Generated filename with timestamp and .json extension
     */
    private generateFileName(): string {
        let fileName = "individual_repositories_" + TimeStampCreator.createTimeStamp() + ".json";
        return fileName;
    }

    /**
     * Loads previously saved individual repositories from disk.
     * Reconstructs full IndividualRepository objects from JSON data.
     * 
     * @returns Array of restored IndividualRepository instances
     */
    private loadIndividualRepositories(): IndividualRepository[] {
        let filePath = path.join(this.individualRepositoriesFolder, this.repositoryConfig.individualRepositoryPersist.savedIndividualRepositoriesFileName);

        let fileContent = fs.readFileSync(filePath).toString();

        let individualRepositories: IndividualRepository[] = [];
        for (let individualRepository of JSON.parse(fileContent)) {
            individualRepositories.push(Object.assign(new IndividualRepository, individualRepository));
        }

        return individualRepositories;
    }

    /**
     * Creates a new individual repository instance.
     * Generates a UUID and initializes with empty selection collection.
     * 
     * @param members - Optional array of member identifiers for the repository
     * @returns Newly created IndividualRepository instance
     */
    private createIndividualRepository(members: string[] | undefined): IndividualRepository {
        let uuid = uuidv4();

        let individualSelectionCollection = this.variationGenerator.getEmptyIndividualSelectionCollection();
        return new IndividualRepository(uuid, members, individualSelectionCollection);
    }

    /**
     * Checks if a repository with the given members already exists.
     * Used to prevent duplicate repositories for the same member group.
     * 
     * @param individualRepositories - Array of existing repositories to check
     * @param members - Member list to look for
     * @returns true if a repository with these members exists, false otherwise
     */
    private doesIndividualRepositoryExist(individualRepositories: IndividualRepository[], members: string[]): boolean {
        for (let individualRepository of individualRepositories) {
            if (individualRepository.compareMembers(members)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Updates selection collections for all repositories.
     * Ensures each repository has current selection options.
     * 
     * @param individualRepositories - Repositories to update
     */
    private updateIndividualSelectionCollections(individualRepositories: IndividualRepository[]) {
        for (let individualRepository of individualRepositories) {
            individualRepository.individualSelectionCollection = this.variationGenerator.getIndividualSelectionCollection(individualRepository.individualSelectionCollection!);
        }
    }

    /**
     * Generates and applies variations for all repositories.
     * Creates unique content variations based on repository ID and selections.
     * 
     * @param individualRepositories - Repositories to generate variations for
     */
    private updateIndividualVariations(individualRepositories: IndividualRepository[]) {
        for (let individualRepository of individualRepositories) {
            individualRepository.individualVariation = this.variationGenerator.generateIndividualVariation({ RepositoryId: individualRepository.id! }, individualRepository.individualSelectionCollection!);
            Logger.getInstance().debug("Generated variables for repository", individualRepository.id!);
            Logger.getInstance().debug(individualRepository.individualVariation);
        }
    }

    /**
     * Creates or loads individual repositories based on configuration.
     * Handles both member-based and count-based repository creation.
     * 
     * @returns Array of created/loaded repositories
     */
    private createIndividualRepositories(): IndividualRepository[] {
        Logger.getInstance().debug("Load existing individual repositories");
        let individualRepositories: IndividualRepository[] = this.repositoryConfig.individualRepositoryPersist.useSavedIndividualRepositories 
                                                                ? this.loadIndividualRepositories() : [];
        if (this.repositoryConfig.repository.repositoryMembers.length == 0) {
            Logger.getInstance().debug("Create missing individual repositories without members");
            for (let i = individualRepositories.length; i < this.repositoryConfig.repository.repositoryCount; i++) {
                individualRepositories.push(this.createIndividualRepository(undefined));
            }
        } else {
            Logger.getInstance().debug("Create missing individual repositories with members");
            for (let members of this.repositoryConfig.repository.repositoryMembers) {
                if (!this.doesIndividualRepositoryExist(individualRepositories, members)) {
                    individualRepositories.push(this.createIndividualRepository(members));
                }
            }
        }

        return individualRepositories;
    }

    /**
     * Filters repositories based on configuration settings.
     * Applies either count-based or member-based filtering.
     * 
     * @param individualRepositories - Repositories to filter
     * @returns Filtered array of repositories
     */
    private filterIndividualRepositories(individualRepositories: IndividualRepository[]): IndividualRepository[] {
        if (this.repositoryConfig.repository.repositoryMembers.length == 0) {
            Logger.getInstance().debug("Filter existing individual repositories by count");
            individualRepositories = individualRepositories.splice(0, this.repositoryConfig.repository.repositoryCount);
        } else {
            Logger.getInstance().debug("Filter existing individual repositories by members");
            let tmpIndividualRepositories: IndividualRepository[] = [];
            for (let individualRepository of individualRepositories) {
                for (let members of this.repositoryConfig.repository.repositoryMembers) {
                    if (individualRepository.compareMembers(members)) {
                        tmpIndividualRepositories.push(individualRepository);
                        break;
                    }
                }
            }
            individualRepositories = tmpIndividualRepositories;
        }
        return individualRepositories;
    }

    /**
     * Main method to retrieve all required individual repositories.
     * Performs the complete workflow:
     * 1. Creates/loads repositories
     * 2. Updates selection collections
     * 3. Persists repositories
     * 4. Filters based on configuration
     * 5. Generates variations
     * 
     * @returns Array of fully configured individual repositories
     */
    public getIndividualRepositories(): IndividualRepository[] {
        let individualRepositories: IndividualRepository[] = this.createIndividualRepositories();
        this.updateIndividualSelectionCollections(individualRepositories);
        this.saveIndividualRepositories(individualRepositories);
        
        let filteredIndividualRepositories: IndividualRepository[] = this.filterIndividualRepositories(individualRepositories);
        this.updateIndividualVariations(filteredIndividualRepositories);
        return filteredIndividualRepositories;
    }
}