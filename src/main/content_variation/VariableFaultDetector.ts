import { ConfigManager } from "../config/ConfigManager";
import { RepositoryFile } from "../content_manager/RepositoryFile";
import { Logger } from "../logging/Logger";
import { IndividualRepository } from "../repository_creation/IndividualRepository";
import { NestedObjectVariationRecord } from "./config_records/object/NestedObjectVariationRecord";
import { VariationsConfig } from "./config_records/VariationsConfig";
import { ContentReplacer } from "./ContentReplacer";
import { IndividualVariation } from "./IndividualVariation";
import { VariablePreProcessor } from "./variable_processor/pre_processor/VariablePreProcessor";

/**
 * Detects potential issues with variable replacements in repository files.
 * Checks for remaining delimiters and unauthorized variable values.
 * Supports configurable ignore lists and type-based filtering.
 */
export class VariableFaultDetector {

    /** List of file identifiers that should be ignored during fault detection */
    private ignoreFilesContains = [ "norepo" ];

    /** Delimiter used to identify variables in content */
    private readonly variableDelimiter = ConfigManager.getInstance().getOriginRepositoryConfig().variables.variableDelimiter;

    /** Configuration for the origin repository */
    private originRepositoryConfig = ConfigManager.getInstance().getOriginRepositoryConfig();
        
    /** List of variable values that are not allowed in the content */
    private blackListedVariableValues: string[] = [];

    /**
     * Creates a new VariableFaultDetector instance.
     * Initializes blacklisted values and ignore file patterns.
     * 
     * @param individualRepository - Repository to check for variable faults
     */
    constructor(private individualRepository: IndividualRepository) {
        this.identifyBlackListedVariableValues(individualRepository.individualVariation!);
        this.identifyIgnoreFilesContains();
    }

    /**
     * Identifies and stores blacklisted variable values by comparing all possible values
     * against those used in the individual variation and the ignore list.
     * 
     * @param individualVariation - The individual's variable variations to check against
     */
    public identifyBlackListedVariableValues(individualVariation: IndividualVariation) {
        let allVariableValues = this.getAllVariableValues();
        let allIndividualVariableValues = this.getAllVariableValuesFromIndividualVariation(individualVariation);
        
        this.blackListedVariableValues = [];
        for (let variableValue of allVariableValues) {
            if (!this.variableValueListContainsItem(allIndividualVariableValues, variableValue) 
                && !this.variableValueListContainsItem(this.originRepositoryConfig.warnings.variableValueWarnings.ignoreList, variableValue)) {
                this.blackListedVariableValues.push(variableValue);
            }
        }
    }

    /**
     * Loads additional file patterns to ignore from the configuration.
     * These patterns are used to exclude certain files from fault detection.
     */
    public identifyIgnoreFilesContains() {
        const ignoreFileList = this.originRepositoryConfig?.warnings?.variableValueWarnings?.ignoreFileList;
        if (Array.isArray(ignoreFileList)) {
            for (let filename of ignoreFileList) {
                this.ignoreFilesContains.push(filename);
            }
        }
    }

    /**
     * Extracts all variable values from an individual variation configuration.
     * 
     * @param individualVariation - The variation configuration to process
     * @returns Array of all variable values found in the variation
     */
    private getAllVariableValuesFromIndividualVariation(individualVariation: IndividualVariation): string[] {
        let allIndividualVariableValues: string[] = []; 

        for (let variableSetKey in individualVariation) {
            let variableSet = individualVariation[variableSetKey];
            for (let variableKey in variableSet) {
                let variableValue = variableSet[variableKey];
                allIndividualVariableValues.push(variableValue);
            }
        }

        return allIndividualVariableValues;
    }

    /**
     * Retrieves all possible variable values from the configuration.
     * Combines values from both object and logic records.
     * 
     * @returns Array of all possible variable values
     */
    private getAllVariableValues(): string[] {
        let variablePreProcessor = new VariablePreProcessor();
        let preProcessedVariationsConfig = variablePreProcessor.processVariationsConfig(
            ConfigManager.getInstance().getVariableExtensionsConfig(), 
            ConfigManager.getInstance().getVariationsConfig()); 

        let allVariableValues: string[] = []; 

        allVariableValues = allVariableValues.concat(this.getAllVariableValuesFromObjectRecords(preProcessedVariationsConfig));
        allVariableValues = allVariableValues.concat(this.getAllVariableValuesFromLogicRecords(preProcessedVariationsConfig));

        return allVariableValues;
    }

    /**
     * Extracts variable values from object variation records.
     * 
     * @param variationsConfig - Configuration containing object records
     * @returns Array of variable values found in object records
     */
    private getAllVariableValuesFromObjectRecords(variationsConfig: VariationsConfig): string[] {
        let allObjectVariableValues: string[] = []; 

        for (let objectRecord of variationsConfig.objects) {
            for (let objectVariation of objectRecord.objectVariations) {
                this.getAllVariableValuesFromVariationRecord(objectVariation, allObjectVariableValues);
            }
        }

        return allObjectVariableValues;
    }

    /**
     * Recursively extracts variable values from a nested variation record.
     * 
     * @param variationRecord - The nested record to process
     * @param allVariationRecordValues - Accumulator for found values
     * @returns Updated array of variable values
     */
    private getAllVariableValuesFromVariationRecord(variationRecord: NestedObjectVariationRecord, allVariationRecordValues: string[]) {
        for (let variableKey in variationRecord) {
            let variableValue = variationRecord[variableKey];
            if (variableValue instanceof Object) {
                this.getAllVariableValuesFromVariationRecord(variableValue, allVariationRecordValues);
            } else {
                if (!allVariationRecordValues.includes(String(variableValue))) {
                    allVariationRecordValues.push(String(variableValue));
                }
            }
        }

        return allVariationRecordValues;
    }

    /**
     * Extracts variable values from logic variation records.
     * 
     * @param variationsConfig - Configuration containing logic records
     * @returns Array of variable values found in logic records
     */
    private getAllVariableValuesFromLogicRecords(variationsConfig: VariationsConfig): string[] {
        let allLogicVariableValues: string[] = []; 

        for (let logicRecord of variationsConfig.logic) {
            for (let logicVariation of logicRecord.logicVariations) {
                for (let variableKey in logicVariation) {
                    if (variableKey !== "id") {
                        let variableValue = logicVariation[variableKey];
                        if (!allLogicVariableValues.includes(String(variableValue))) { // TODO does this work for type object?
                            allLogicVariableValues.push(String(variableValue));
                        }
                    }
                }
            }
        }

        return allLogicVariableValues;
    }

    /**
     * Checks a repository file for variable-related issues.
     * Detects remaining delimiters and blacklisted variable values.
     * Logs warnings for any issues found.
     * 
     * @param repositoryFile - The file to check for faults
     */
    public detectFaults(repositoryFile: RepositoryFile) {
        if (!this.ignoreFile(repositoryFile)) {
            if (this.variableDelimiter.length > 0 && (this.detectRemainingDelimiter(repositoryFile.path) || this.detectRemainingDelimiter(repositoryFile.content))) {
                Logger.getInstance().warning(`There are remaining "${this.variableDelimiter}" in the file: ${repositoryFile.path}`, this.individualRepository.id!, true);
            }

            for (let blackListedVariableValue of this.blackListedVariableValues) {
                if (repositoryFile.path.includes(blackListedVariableValue) || repositoryFile.content.includes(blackListedVariableValue)) {
                    Logger.getInstance().warning(`The variable value "${blackListedVariableValue}" should not be contained in the file: ${repositoryFile.path}`, this.individualRepository.id!, true);
                }
            }
        }
    }

    /**
     * Checks for unprocessed variable delimiters in content.
     * Ignores matches that are in the configured ignore list.
     * 
     * @param content - The content to check for remaining delimiters
     * @returns true if unauthorized delimiters are found, false otherwise
     */
    private detectRemainingDelimiter(content: string): boolean {
        let escapeVariableDelimiter = ContentReplacer.escapeVariableDelimiter(this.variableDelimiter);
        let regex = new RegExp(`\\S*${escapeVariableDelimiter}+\\S*`, "g");
        let resultArray = content.match(regex);

        if (!resultArray) {
            return false;
        }

        for (let result of resultArray) {
            if (!this.variableValueContainsList(this.originRepositoryConfig.warnings.variableValueWarnings.ignoreList, result)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if a value exists in a list of variable values using case-insensitive regex matching.
     * 
     * @param variableValues - List of variable values to search in
     * @param checkVariableValue - Value to search for
     * @returns true if the value is found in the list, false otherwise
     */
    private variableValueListContainsItem(variableValues: string[], checkVariableValue: string): boolean {
        for (let variableValue of variableValues) {
            if (new RegExp(checkVariableValue, "i").test(variableValue)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a value contains any of the patterns from a list of variable values.
     * Uses case-insensitive regex matching.
     * 
     * @param variableValues - List of patterns to check against
     * @param checkVariableValue - Value to check for pattern matches
     * @returns true if any pattern matches the value, false otherwise
     */
    private variableValueContainsList(variableValues: string[], checkVariableValue: string): boolean {
        for (let variableValue of variableValues) {
            if (new RegExp(variableValue, "i").test(checkVariableValue)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determines if a file should be excluded from fault detection.
     * Checks against ignore patterns and file type whitelist.
     * 
     * @param repositoryFile - File to check for exclusion
     * @returns true if the file should be ignored, false if it should be checked
     */
    private ignoreFile(repositoryFile: RepositoryFile): boolean {
        for (let ignoreFileIdentifier of this.ignoreFilesContains) {
            if (repositoryFile.path.includes(ignoreFileIdentifier)) {
                return true;
            }
        }

        for (let whiteListIdentifier of this.originRepositoryConfig.warnings.variableValueWarnings.typeWhiteList) {
            if (repositoryFile.path.endsWith(`.${whiteListIdentifier}`)) {
                return false;
            }
        }

        return true;
    }
}