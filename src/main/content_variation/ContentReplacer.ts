import { ConfigManager } from "../config/ConfigManager";
import { RepositoryFile } from "../content_manager/RepositoryFile";
import { IndividualRepository } from "../repository_creation/IndividualRepository";
import { IndividualVariation } from "./IndividualVariation";
import { ReplaceVariable } from "./ReplaceVariable";
import { VariableFaultDetector } from "./VariableFaultDetector";
import { VariationGenerator } from "./VariationGenerator";

/**
 * Handles the replacement of variables in repository files with their corresponding values.
 * Supports variable replacement in both file paths and content, with configurable delimiters.
 * Can optionally detect variable-related faults using VariableFaultDetector.
 */
export class ContentReplacer {

    /** Temporary character separator used when no variable delimiter is specified */
    private readonly tmpCharSeparator = "\\$\\$\\$";

    /** Delimiter used to identify variables in content, configured via ConfigManager */
    private readonly variableDelimiter = ConfigManager.getInstance().getOriginRepositoryConfig().variables.variableDelimiter;

    /** Optional detector for variable-related faults */
    private variableFaultDetector: VariableFaultDetector | undefined = undefined;

    /** Collection of variables to be replaced and their corresponding values */
    private replaceVariables: ReplaceVariable[];

    /**
     * Creates a new ContentReplacer instance.
     * Initializes replace variables and optionally sets up fault detection.
     * 
     * @param individualRepository - Repository containing individual-specific variations
     */
    constructor(individualRepository: IndividualRepository) {
        this.replaceVariables = this.calculateReplaceVariables(individualRepository.individualVariation!);

        if (ConfigManager.getInstance().getRepositoryConfig().general.variateRepositories
            && ConfigManager.getInstance().getRepositoryConfig().general.activateVariableValueWarnings) {
            this.variableFaultDetector = new VariableFaultDetector(individualRepository);
        }
    }

    /**
     * Replaces variables in both the path and content of a repository file.
     * If fault detection is enabled, checks for variable-related issues.
     * 
     * @param repositoryFile - The repository file to process
     * @returns The processed repository file with variables replaced
     */
    public replacePathAndContent(repositoryFile: RepositoryFile): RepositoryFile {
        repositoryFile.path = this.replaceContent(repositoryFile.path); 
        repositoryFile.content = this.replaceContent(repositoryFile.content); 

        if (this.variableFaultDetector) {
            this.variableFaultDetector.detectFaults(repositoryFile);
        }

        return repositoryFile;
    }

    /**
     * Replaces all variables in the given content with their corresponding values.
     * Handles special cases for empty delimiters using temporary separators.
     * 
     * @param oldContent - Content containing variables to be replaced
     * @returns Content with all variables replaced by their values
     */
    private replaceContent(oldContent: string) {
        let escapeVariableDelimiter = ContentReplacer.escapeVariableDelimiter(this.variableDelimiter);
        var newContent = oldContent;

        for (let replaceVariable of this.replaceVariables) {
            newContent = newContent.replace(new RegExp(`${escapeVariableDelimiter}${replaceVariable.name}${escapeVariableDelimiter}`, "gm"), replaceVariable.value);
        }

        if (this.variableDelimiter.length == 0) {
            newContent = newContent.replace(new RegExp(`${this.tmpCharSeparator}`, "gm"), "");
        }

        return newContent;
    }

    /**
     * Generates replace variables from the individual variation configuration.
     * Sorts variables by name length if no delimiter is specified or using divide char.
     * 
     * @param individualVariation - Configuration specifying variable replacements
     * @returns Array of ReplaceVariable objects containing name-value pairs
     */
    private calculateReplaceVariables(individualVariation: IndividualVariation): ReplaceVariable[] {
        let replaceVariables: ReplaceVariable[] = [];

        for (let preIdentifier in individualVariation) {
            for (let key in individualVariation[preIdentifier]) {
                replaceVariables.push({ 
                    name: key, 
                    value: this.calculateReplaceVariableValue(individualVariation[preIdentifier][key]) 
                });
            }
        }

        if (this.variableDelimiter.length == 0 || this.variableDelimiter == VariationGenerator.divideChar) {
            replaceVariables.sort((a, b) => b.name.length - a.name.length);
        }
        return replaceVariables;
    }

    /**
     * Processes a replacement value based on delimiter configuration.
     * If no delimiter is specified, wraps each character with temporary separators.
     * 
     * @param value - The original replacement value
     * @returns Processed replacement value with appropriate separators
     */
    private calculateReplaceVariableValue(value: string): string {
        if (this.variableDelimiter.length > 0) {
            return value;
        }

        let newValue = this.tmpCharSeparator;
        for (let i = 0; i < value.length; i++) {
            newValue += value.charAt(i) + this.tmpCharSeparator;
        }

        return newValue;
    }

    /**
     * Escapes special characters in the variable delimiter for use in regular expressions.
     * Single-character delimiters are escaped with a backslash.
     * 
     * @param delimiter - The variable delimiter to escape
     * @returns Escaped version of the delimiter safe for regex use
     */
    public static escapeVariableDelimiter(delimiter: string): string {
        return delimiter.length == 1 ? `\\${delimiter}` : delimiter;
    }
}