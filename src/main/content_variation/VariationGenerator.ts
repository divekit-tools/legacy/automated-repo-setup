import { RelationVariableGenerator } from './variable_generator/RelationVariableGenerator';
import { ObjectVariableGenerator } from './variable_generator/ObjectVariableGenerator';
import { RepositoryMetaData } from './RepositoryMetaData';
import { LogicVariableGenerator } from './variable_generator/LogicVariableGenerator';
import { IndividualSelectionCollection } from './selections/IndividualSelectionCollection';
import { VariationsConfig } from './config_records/VariationsConfig';
import { IndividualVariation } from './IndividualVariation';
import { ConfigManager } from '../config/ConfigManager';
import { VariablePreProcessor } from './variable_processor/pre_processor/VariablePreProcessor';
import { VariablePostProcessor } from './variable_processor/post_processor/VariablePostProcessor';

/**
 * Generates and manages variations in repository content.
 * Coordinates different types of variations (object, relation, logic) and their processing.
 * Handles pre-processing of configurations, generation of variations, and post-processing of results.
 */
export class VariationGenerator {

    /** Group identifier for general metadata variables */
    public static readonly metaDataGroupId = "General";
    
    /** Character used to divide variable parts in generated names */
    public static readonly divideChar = "_";

    /** Processed configuration for variations */
    private variationsConfig?: VariationsConfig;

    /** Processor for preparing variation configurations */
    private variablePreProcessor: VariablePreProcessor;

    /** Generator for object-based variations */
    private objectVariableGenerator: ObjectVariableGenerator; // TODO better abstraction of variable generators
    
    /** Generator for relationship-based variations */
    private relationVariableGenerator: RelationVariableGenerator;
    
    /** Generator for logic-based variations */
    private logicVariableGenerator: LogicVariableGenerator;

    /** Processor for finalizing generated variations */
    private variablePostProcessor: VariablePostProcessor;

    /**
     * Creates a new VariationGenerator instance.
     * Initializes all required processors and generators.
     * Loads and pre-processes variation configuration if repository variation is enabled.
     */
    constructor() {
        this.variablePreProcessor = new VariablePreProcessor();

        this.objectVariableGenerator = new ObjectVariableGenerator(VariationGenerator.divideChar);
        this.relationVariableGenerator = new RelationVariableGenerator(VariationGenerator.divideChar);
        this.logicVariableGenerator = new LogicVariableGenerator(VariationGenerator.divideChar);

        this.variablePostProcessor = new VariablePostProcessor([VariationGenerator.metaDataGroupId]);

        if (ConfigManager.getInstance().getRepositoryConfig().general.variateRepositories) {
            this.variationsConfig = this.variablePreProcessor.processVariationsConfig(
                ConfigManager.getInstance().getVariableExtensionsConfig(), 
                ConfigManager.getInstance().getVariationsConfig()
            ); 
        }
    } 

    /**
     * Creates an empty selection collection for a new individual repository.
     * Initializes all selection types (object, relation, logic) as empty objects.
     * 
     * @returns New empty individual selection collection
     */
    public getEmptyIndividualSelectionCollection(): IndividualSelectionCollection {
        return { 
            individualObjectSelection: {}, 
            individualRelationSelection: {}, 
            individualLogicSelection: {} 
        };
    }

    /**
     * Updates or creates selections for an individual repository.
     * Processes object, relation, and logic selections based on current configuration.
     * 
     * @param individualSelectionCollection - Existing selection collection to update
     * @returns Updated selection collection with current options
     */
    public getIndividualSelectionCollection(individualSelectionCollection: IndividualSelectionCollection): IndividualSelectionCollection {
        if (this.variationsConfig) {
            individualSelectionCollection.individualObjectSelection = this.objectVariableGenerator.getIndividualObjectSelection(this.variationsConfig.objects, individualSelectionCollection.individualObjectSelection);
            individualSelectionCollection.individualRelationSelection = this.relationVariableGenerator.getIndividualRelationSelection(this.variationsConfig.relations, individualSelectionCollection.individualRelationSelection);
            individualSelectionCollection.individualLogicSelection = this.logicVariableGenerator.getIndividualLogicSelection(this.variationsConfig.logic, individualSelectionCollection.individualLogicSelection);
        }
        return individualSelectionCollection;
    }

    /**
     * Generates variations for an individual repository.
     * Creates variations based on repository metadata and selected options.
     * Applies all necessary processing steps:
     * 1. Generates metadata variables
     * 2. Processes object variations
     * 3. Applies relationship variations
     * 4. Adds logic variations
     * 5. Post-processes the complete variation
     * 
     * @param repositoryMetaData - Metadata for the repository (e.g., ID)
     * @param individualSelectionCollection - Selected variation options
     * @returns Complete set of variations for the repository
     */
    public generateIndividualVariation(repositoryMetaData: RepositoryMetaData, individualSelectionCollection: IndividualSelectionCollection): IndividualVariation { // TODO error handling?
        let individualVariation: IndividualVariation = {};
        individualVariation = this.generateIndividualRepositoryMetaDataVariables(repositoryMetaData, individualVariation);

        if (this.variationsConfig) {
            let individualObjects = this.objectVariableGenerator.getSelectedObjects(this.variationsConfig.objects, individualSelectionCollection.individualObjectSelection);

            individualVariation = this.objectVariableGenerator.generateIndividualObjectVariables(
                individualObjects, 
                individualVariation);
    
            individualVariation = this.relationVariableGenerator.generateIndividualRelationVariables(
                individualObjects, 
                ConfigManager.getInstance().getRelationsConfig(), 
                this.variationsConfig.relations, 
                individualVariation,
                individualSelectionCollection.individualRelationSelection);

            individualVariation = this.logicVariableGenerator.generateIndividualLogicVariables(
                this.variationsConfig.logic, 
                individualVariation,
                individualSelectionCollection.individualLogicSelection);

            individualVariation = this.variablePostProcessor.processIndividualVariation(individualVariation);
        }

        return individualVariation;
    }  
    
    /**
     * Generates variation variables from repository metadata.
     * Creates variables under the general metadata group identifier.
     * 
     * @param repositoryMetaData - Source metadata to convert to variables
     * @param individualVariation - Variation object to add metadata variables to
     * @returns Updated variation object including metadata variables
     */
    private generateIndividualRepositoryMetaDataVariables(repositoryMetaData: RepositoryMetaData, individualVariation: IndividualVariation): IndividualVariation {
        let preIdentifier = VariationGenerator.metaDataGroupId;
        individualVariation[preIdentifier] = {};

        for (let [key, value] of Object.entries(repositoryMetaData)) {
            individualVariation[preIdentifier][key] = String(value);    
        }

        return individualVariation;
    }
}