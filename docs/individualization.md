# Implementierung der Individualisierung in `automated-repo-setup`

Dieses Dokument beschreibt die Implementierung der Individualisierungsfunktionalität innerhalb des `automated-repo-setup` Tools. Es deckt die Konfigurationserstellung, das Lesen der Konfiguration, die Dateimanipulation und die Variationsgenerierung ab.

## 1. Konfigurationserstellung und -ladung

Die Konfiguration für die Individualisierung wird hauptsächlich durch JSON-Dateien gesteuert. Es gibt mehrere Konfigurationsdateien, die verschiedene Aspekte der Individualisierung steuern:

*   **`repositoryConfig.json`**: Enthält allgemeine Einstellungen, die Art der Repositories (lokal oder GitLab), die Erstellung von Test-Repositories, die Aktivierung der Variation, die maximale Anzahl gleichzeitiger Worker und den globalen Loglevel.
    ```json
    {
        "general": {
            "localMode": true,
            "createTestRepository": true,
            "variateRepositories": true,
            "deleteSolution": false,
            "activateVariableValueWarnings": true,
            "maxConcurrentWorkers": 1,
            "globalLogLevel": "Info"
        },
        "repository": {
            "repositoryName": "st2-praktikum",
            "repositoryCount": 0,
            "repositoryMembers": [
                ["st2-praktikum"]
            ]
        },
        "individualRepositoryPersist": {
            "useSavedIndividualRepositories": false,
            "savedIndividualRepositoriesFileName": "ms-spring.json"
        },
        "local": {
            "originRepositoryFilePath": "",
            "subsetPaths": []
        },
        "remote": {
            "originRepositoryId": 1012,
            "codeRepositoryTargetGroupId": 161,
            "testRepositoryTargetGroupId": 170,
            "deleteExistingRepositories": false,
            "addUsersAsGuests": false
        },
        "overview": {
            "generateOverview": true,
            "overviewRepositoryId": 1018,
            "overviewFileName": "spring-nachholpraktikum"
        }
    }
    ```

*   **`originRepositoryConfig.json`**: Spezifische Konfigurationen für das Ursprungsrepository, einschließlich Variablenersetzungsdetails und Einstellungen zur Lösungsentfernung.

*   **`variationsConfig.json`**: Definiert die spezifischen Variationen, die auf Objekte, Beziehungen und Logik angewendet werden sollen. (Ähnlich der Struktur in `docs/ai/individualization.md`)

*   **`relationsConfig.json`**: Konfiguriert Beziehungen zwischen verschiedenen Variationen.

*   **`variableExtensionsConfig.json`**: Ermöglicht zusätzliche Erweiterungen und Anpassungen der Variablen.

Der `ConfigManager` ist für das Laden und Bereitstellen dieser Konfigurationen verantwortlich. Er verwendet eine Singleton-Instanz, um sicherzustellen, dass die Konfigurationen im gesamten Tool konsistent zugänglich sind.

```typescript
startLine: 12
endLine: 24
```

Die `loadConfigsFromOriginRepoFiles`-Methode lädt Konfigurationen direkt aus den Dateien des Ursprungsrepositories, was eine dynamische Konfiguration basierend auf dem Inhalt des Ursprungsrepositories ermöglicht.

```typescript
public loadConfigsFromOriginRepoFiles(originRepositoryFiles: RepositoryFile[]) {
    let newOriginRepositoryConfig = this.loadConfigFromOriginRepoFiles(originRepositoryFiles, "originRepositoryConfig");
    this.configs.originRepositoryConfig = newOriginRepositoryConfig ? newOriginRepositoryConfig : this.configs.originRepositoryConfig;

    let newRelationsConfig = this.loadConfigFromOriginRepoFiles(originRepositoryFiles, "relationsConfig");
    this.configs.relationsConfig = newRelationsConfig ? newRelationsConfig : this.configs.relationsConfig;

    let newVariableExtensionsConfig = this.loadConfigFromOriginRepoFiles(originRepositoryFiles, "variableExtensionsConfig");
    this.configs.variableExtensionsConfig = newVariableExtensionsConfig ? newVariableExtensionsConfig : this.configs.variableExtensionsConfig;

    let newVariationsConfig = this.loadConfigFromOriginRepoFiles(originRepositoryFiles, "variationsConfig");
    this.configs.variationsConfig = newVariationsConfig ? newVariationsConfig : this.configs.variationsConfig;
}
```

## 2. Variationsgenerierung

Die `VariationGenerator`-Klasse ist für die Erzeugung von individuellen Variationen für jedes Repository verantwortlich.

```typescript
export class VariationGenerator {
```

Die `generateIndividualVariation`-Methode kombiniert Metadaten des Repositories mit ausgewählten Variationen, um einen vollständigen Satz von Variationen für ein einzelnes Repository zu erstellen.

```typescript
public generateIndividualVariation(repositoryMetaData: RepositoryMetaData, individualSelectionCollection: IndividualSelectionCollection): IndividualVariation { // TODO error handling?
    let individualVariation: IndividualVariation = {};
    individualVariation = this.generateIndividualRepositoryMetaDataVariables(repositoryMetaData, individualVariation);

    if (this.variationsConfig) {
        let individualObjects = this.objectVariableGenerator.getSelectedObjects(this.variationsConfig.objects, individualSelectionCollection.individualObjectSelection);

        individualVariation = this.objectVariableGenerator.generateIndividualObjectVariables(
            individualObjects, 
            individualVariation);

        individualVariation = this.relationVariableGenerator.generateIndividualRelationVariables(
            individualObjects, 
            ConfigManager.getInstance().getRelationsConfig(), 
            this.variationsConfig.relations, 
            individualVariation,
            individualSelectionCollection.individualRelationSelection);

        individualVariation = this.logicVariableGenerator.generateIndividualLogicVariables(
            this.variationsConfig.logic, 
            individualVariation,
            individualSelectionCollection.individualLogicSelection);

        individualVariation = this.variablePostProcessor.processIndividualVariation(individualVariation);
    }

    return individualVariation;
}
```

Dieser Prozess umfasst:

1.  Generierung von Metadatenvariablen.
2.  Verarbeitung von Objektvariationen.
3.  Anwendung von Beziehungs-Variationen.
4.  Hinzufügen von Logikvariationen.
5.  Nachbearbeitung der Variationen.

Die `IndividualRepositoryManager`-Klasse verwaltet die Erstellung, Persistierung und Variation einzelner Repositories.

```typescript
export class IndividualRepositoryManager {

    /** Path to the folder containing persisted individual repository data */
    private readonly individualRepositoriesFolder = path.join('./resources/individual_repositories');

    /** Configuration for repository operations */
    private repositoryConfig = ConfigManager.getInstance().getRepositoryConfig();
    
    /** Generator for creating variations in repository content */
    private variationGenerator: VariationGenerator;

    /**
     * Creates a new IndividualRepositoryManager instance.
     * Initializes the variation generator and ensures storage directory exists.
     */
    constructor() {
        this.variationGenerator = new VariationGenerator();
        this.initializeIndividualRepositoriesFolder();
    }
```

Sie lädt vorhandene Repositories oder erstellt neue und wendet Variationen an. Die `updateIndividualVariations`-Methode generiert und wendet Variationen für alle Repositories an.

```typescript
private updateIndividualVariations(individualRepositories: IndividualRepository[]) {
    for (let individualRepository of individualRepositories) {
        individualRepository.individualVariation = this.variationGenerator.generateIndividualVariation({ RepositoryId: individualRepository.id! }, individualRepository.individualSelectionCollection!);
        Logger.getInstance().debug("Generated variables for repository", individualRepository.id!);
        Logger.getInstance().debug(individualRepository.individualVariation);
    }
}
```

## 3. Dateimanipulation

Die Dateimanipulation wird hauptsächlich durch den `ContentProvider` und den `ContentReplacer` durchgeführt.

Der `ContentProvider` verwaltet die Bereitstellung und Verteilung von Inhalten an Code- und Test-Repositories. Er identifiziert Dateien, ersetzt Inhalte und richtet die Repositories ein.

```typescript
export class ContentProvider {
    
    /** Prefix used for special file identifiers */
    private readonly filePreIdentifier = "_";

    /** Identifier for files that should go to the code repository */
    private readonly codeRepoFileIdentifier = this.filePreIdentifier + "coderepo";
    
    /** Identifier for files that should go to the test repository */
    private readonly testRepoFileIdentifier = this.filePreIdentifier + "testrepo";
    
    /** Identifier for files that should not go to any repository */
    private readonly noRepoFileIdentifier = this.filePreIdentifier + "norepo";

    /** Manager for handling file manipulations */
    private fileManipulatorManager: FileManipulatorManager;

    /**
     * Creates a new ContentProvider instance.
     * 
     * @param repositoryAdapter - Adapter for interacting with the repository system
     * @param individualRepository - Repository containing individual-specific information
     */
    constructor(public readonly repositoryAdapter: RepositoryAdapter, public readonly individualRepository: IndividualRepository) {
        this.fileManipulatorManager = new FileManipulatorManager();    
    }
```

Die `provideRepositoriesWithContent`-Methode verteilt die Inhalte an die entsprechenden Repositories, basierend auf Datei-Identifikatoren (z.B. `_coderepo`, `_testrepo`).

```typescript
public async provideRepositoriesWithContent(originRepositoryFiles: RepositoryFile[], codeRepositoryName: string, testRepositoryName: string): Promise<void> {

    await this.prepareRepositoryManager(codeRepositoryName, testRepositoryName);

    let codeRepositoryFiles: RepositoryFile[] = [];
    let testRepositoryFiles: RepositoryFile[] = [];

 {
        // Commit file to code, test repo or to both
        if (individualRepositoryFile.path.includes(this.codeRepoFileIdentifier)) {
            individualRepositoryFile.path = individualRepositoryFile.path.replace(this.codeRepoFileIdentifier, '');
            individualRepositoryFile.content = individualRepositoryFile.content.replace(new RegExp(this.codeRepoFileIdentifier, "g"), "");
            codeRepositoryFiles.push(individualRepositoryFile);
        } else if (individualRepositoryFile.path.includes(this.testRepoFileIdentifier)) {
            individualRepositoryFile.path = individualRepositoryFile.path.replace(this.testRepoFileIdentifier, '');
            individualRepositoryFile.content = individualRepositoryFile.content.replace(new RegExp(this.testRepoFileIdentifier, "g"), "");
            testRepositoryFiles.push(individualRepositoryFile);
        } else if (!individualRepositoryFile.path.includes(this.noRepoFileIdentifier)) {
            codeRepositoryFiles.push({ path: individualRepositoryFile.path, content: individualRepositoryFile.content, encoding: individualRepositoryFile.encoding });
            testRepositoryFiles.push({ path: individualRepositoryFile.path, content: individualRepositoryFile.content, encoding: individualRepositoryFile.encoding });
        }
    }

    codeRepositoryFiles = this.filterIndividualRepositoryFiles(codeRepositoryFiles); // TODO better solution instead of 2 times filtering
    testRepositoryFiles = this.filterIndividualRepositoryFiles(testRepositoryFiles);

    await this.repositoryAdapter.provideContentToCodeRepository(codeRepositoryFiles);
    if (ConfigManager.getInstance().getRepositoryConfig().general.createTestRepository) {
        await this.repositoryAdapter.provideContentToTestRepository(testRepositoryFiles);
    }
}
```

Der `ContentReplacer` ist für das Ersetzen von Variablen in Repository-Dateien durch ihre entsprechenden Werte verantwortlich.

```typescript
export class ContentReplacer {

    /** Temporary character separator used when no variable delimiter is specified */
    private readonly tmpCharSeparator = "\\$\\$\\$";

    /** Delimiter used to identify variables in content, configured via ConfigManager */
    private readonly variableDelimiter = ConfigManager.getInstance().getOriginRepositoryConfig().variables.variableDelimiter;

    /** Optional detector for variable-related faults */
    private variableFaultDetector: VariableFaultDetector | undefined = undefined;

    /** Collection of variables to be replaced and their corresponding values */
    private replaceVariables: ReplaceVariable[];

    /**
     * Creates a new ContentReplacer instance.
     * Initializes replace variables and optionally sets up fault detection.
     * 
     * @param individualRepository - Repository containing individual-specific variations
     */
    constructor(individualRepository: IndividualRepository) {
        this.replaceVariables = this.calculateReplaceVariables(individualRepository.individualVariation!);

        if (ConfigManager.getInstance().getRepositoryConfig().general.variateRepositories
            && ConfigManager.getInstance().getRepositoryConfig().general.activateVariableValueWarnings) {
            this.variableFaultDetector = new VariableFaultDetector(individualRepository);
        }
    }
```

Er unterstützt die Variablenersetzung sowohl in Dateipfaden als auch im Inhalt und verwendet konfigurierbare Trennzeichen. Der `VariableFaultDetector` kann optional Variablenfehler erkennen.

Die `SolutionDeleter`-Klasse ist für das Entfernen von Lösungsbestandteilen aus den Dateien zuständig, basierend auf Konfigurationsparametern.

```typescript
startLine: 8
endLine: 42
```

## 4. Hauptablauf der Individualisierung

Der `RepositoryCreator` orchestriert den gesamten Prozess der Repository-Erstellung und Individualisierung.

```typescript
startLine: 21
endLine: 175
```

Die `generateRepositories`-Methode ist der Haupteinstiegspunkt. Sie führt die folgenden Schritte aus:

1.  Ruft die Dateien des Ursprungsrepositories ab und filtert sie.
    ```typescript
    public async generateRepositories(): Promise<void> {
        let originRepositoryFiles: RepositoryFile[] = await this.retrieveOriginRepositoryFiles();
        originRepositoryFiles = this.filterRepositoryList(originRepositoryFiles)

        let individualRepositoryManager: IndividualRepositoryManager = new IndividualRepositoryManager();
        let individualRepositories: IndividualRepository[] = individualRepositoryManager.getIndividualRepositories();   

        Logger.getInstance().debug("Start preparing environment");
        await this.generateRepositoryAdapter().prepareEnvironment();
        Logger.getInstance().debug("Finished preparing environment");

        let contentProviders = await this.startRepositoryGenerationTasks(originRepositoryFiles, individualRepositories);

        if (ConfigManager.getInstance().getRepositoryConfig().overview.generateOverview) {
            let overviewGenerator = new OverviewGenerator(this.generateRepositoryAdapter());
            overviewGenerator.generateOverviewPage(contentProviders);
        }
    }
    ```
2.  Initialisiert die individuellen Repositories.
3.  Bereitet die Umgebung vor.
4.  Generiert die Repositories parallel.
5.  Erstellt optional eine Übersichtsseite.

Die `createRepository`-Methode erstellt ein einzelnes Repository für einen Benutzer.

```typescript
private async createRepository(originRepositoryFiles: RepositoryFile[], individualRepository: IndividualRepository): Promise<ContentProvider> {
    const codeRepositoryName = `${ConfigManager.getInstance().getRepositoryConfig().repository.repositoryName}_group_${individualRepository.id}`;
    const testRepositoryName = `${ConfigManager.getInstance().getRepositoryConfig().repository.repositoryName}_tests_group_${individualRepository.id}`;

    try { 
        let repositoryAdapter = this.generateRepositoryAdapter(individualRepository);
        let contentProvider = new ContentProvider(repositoryAdapter, individualRepository);
        await contentProvider.provideRepositoriesWithContent(originRepositoryFiles, codeRepositoryName, testRepositoryName); 
        Logger.getInstance().info(`Finished generating repository`, individualRepository.id!);
        return contentProvider;
    } catch (error) {
        let errorMsg = `An error occurred while generating repository`;
        if (individualRepository.members) {
            errorMsg = errorMsg + " (Members: " + individualRepository.getMembersList() + ")";
        }
        Logger.getInstance().error(errorMsg, individualRepository.id!);
        Logger.getInstance().error(<any> error, individualRepository.id!);
        throw error;
    }
}
```

Sie behandelt:

* Repository-Benennung
* Inhaltsbereitstellung
* Fehlerbehandlung und Protokollierung

## Anmerkungen und Optimierungsmöglichkeiten

1. **Vereinheitlichung der Konfiguration:** Die Konfiguration ist über mehrere Dateien verteilt. Eine zentrale Konfigurationsdatei oder ein einheitlicherer Ansatz könnte die Wartbarkeit verbessern.

2. **Abstraktion der Variationslogik:** Die Logik für die Generierung von Objekt-, Beziehungs- und Logikvariationen könnte weiter in separate Klassen oder Module abstrahiert werden, um die Modularität zu erhöhen.

3. **Typsicherheit:** Obwohl TypeScript verwendet wird, gibt es immer noch einige `any`-Typen. Eine stärkere Typisierung könnte die Robustheit und Wartbarkeit verbessern.

4. **Fehlerbehandlung:** Die Fehlerbehandlung ist vorhanden, könnte aber durch spezifischere Fehlertypen und detailliertere Fehlermeldungen verbessert werden.

5. **Performance:** Die parallele Verarbeitung von Repositories ist ein guter Ansatz. Weitere Optimierungen könnten durch Caching von häufig verwendeten Ressourcen oder durch die Optimierung der Dateifilterung erreicht werden.

6. **Go-Code Integration:** Die aktuelle Implementierung ist in TypeScript. Eine engere Integration mit dem Go-Code (z.B. durch gemeinsame Schnittstellen oder Datenstrukturen) könnte die Konsistenz und Wiederverwendbarkeit verbessern. Die Interfaces in `pkg/individualizer/interfaces/interfaces.go` und die Implementierungen in `pkg/individualizer/generator/variation.go` und `pkg/individualizer/manager.go` zeigen, wie dies in Go strukturiert werden könnte.

7. **Bessere Fehlerbehandlung in `VariationGenerator`:**
```typescript
public generateIndividualVariation(repositoryMetaData: RepositoryMetaData, individualSelectionCollection: IndividualSelectionCollection): IndividualVariation { // TODO error handling?
    // ...
}
```
Es ist ein `// TODO: Implementiere die tatsächliche Logik` Kommentar vorhanden.

8. **Verwendung von Interfaces:**
```go
startLine: 19
endLine: 22
```
SelectionCollection ist ein Interface ohne Methoden.
